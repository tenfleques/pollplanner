<?php
include_once("functions.php");
$json = array("authenticated" => false, "token" => "", "username" => "", "error" =>array("en"=>"error", "ru"=> "ошибка"));
class user{
	private $authenticated = false;
	private $myId = -1;
	public function authenticate($token,$refresh=true){
		$sql = "select userid,username,password,hash from users where hash='$token'";
		$q = queryMysql($sql);
		$res = mysqli_fetch_assoc($q["result"]);
		if(isset($res["password"]) && $refresh)//authentication with data promise and token refresh
			return $this->updateHashAndCheckout($res);
		elseif (isset($res["password"])) {
			$this->authenticated = true;
			$this->myId = $id = $res["userid"];
			return array("authenticated" => true, "token" => $res["hash"], "username" => $res["username"], "error" =>array("en"=>"success", "ru"=> "успешно"));
		}
		return array("authenticated" => false, "token" => "", "username" => "", "error" =>array("en"=>"error", "ru"=> "ошибка"));
	}
	private function updateHashAndCheckout($res){
		//update the hash
		$this->authenticated = true;
		$this->myId = $id = $res["userid"];
		$hash = hash("sha256", time().serialize($res));
		$updatesql = queryMysql("update users set hash = '$hash' where userid = '$id'");
		$now = time();
		$data = array("active"=>$this->getEvents(false,"where et > '$now'"));
		if(mysqli_affected_rows($updatesql["link"]) !== -1)
			return array("authenticated" => true, "token" => $hash, "data" => $data, "username" => $res["username"], "error" =>array("en"=>"success", "ru"=> "успешно"));
		else
			return array("authenticated" => true, "token" => $res["hash"], "data" => $data, "username" => $res["username"], "error" =>array("en"=>"success", "ru"=> "успешно"));
	}
	public function login($username,$pass){
		$sql = "select userid,username,password,hash from users where username='$username'";
		$q = queryMysql($sql);
		$res = mysqli_fetch_assoc($q["result"]);
		if(isset($res["password"])){
			if(checkPasswordHash($res["password"],$pass)){
				return $this->updateHashAndCheckout($res);
			}
		}
		return array("authenticated" => false, "token" => "", "username" => "", "error" =>array("en"=>"incorrect username/password", "ru"=> "неверно логин/пароль"));
	}
	public function userExists($username){
		$sql = "select userid from users where username='$username'";
		$q = queryMysql($sql);
		return (mysqli_num_rows($q["result"]) > 0);
	}
	public function signUp($username,$pass){
		$inpass = passHash($pass);
		if($this->userExists($username))
			return array("authenticated" => false, "token" => "", "username" => "$username", "data"=>array(), "error" =>array("en"=>"name in use", "ru"=> "имя занято"));
		$sql = "INSERT INTO users (username,password) VALUES ('$username', '$inpass')";
		$q = queryMysql($sql);
		return $this->login($username,$pass);
	}

	public function getPollResults($eventId){
			$sql = "select users.username, votes.votedtime, votes.action from votes join users on votes.userid = users.userid where votes.eventId = '$eventId'";
			$r = queryMysql($sql);
			$polls = array("times"=>array(), "actions" =>array());
			while($row = mysqli_fetch_assoc($r["result"])){
				$row["action"] = removeTableSanity($row["action"]);
				$row["username"] = removeTableSanity($row["username"]);
				$polls["times"]["time_".$row["votedtime"]][] = $row;
				$polls["actions"]["action_".hash("adler32",$row["action"])][] = $row; //hashing because might contain magical characters that break the naming rules of indices
			}
			return $polls;
	}
	public function getEventAuthor($event){
		$sql = "select users.username from users join event on event.eventauthor=users.userid where event.eventid='$event'";
		$q = queryMysql($sql);
		return mysqli_fetch_row($q["result"])[0];
	}
	public function startEvent($eventname, $phase1duration, $phase2duration){
		if($this->authenticated){
			$now = time();
			$activeEvents = $this->getEvents(false, " where $now < et ");
			if(!$activeEvents){
				$eventduration = $phase1duration + $phase2duration;
				$eventauthor = $this->myId;
				$pct = $now + $phase1duration;
				$et = $now + $eventduration;
				$sql = "insert into event (eventname,eventduration,eventauthor,st,pct,et) values ('$eventname', '$eventduration', '$eventauthor', '$now', '$pct', '$et') on duplicate key update eventid=eventid";
				$q = queryMysql($sql);
				return array("insertedevent" => mysqli_insert_id($q["link"]) > 0, "insertedescription"=>array("en"=>"succesfully saved poll <<$eventname>>", "ru" =>"успешно записан голосовоние <<$eventname>>"));
			}
			return array("insertedevent" => 0,"insertedescription"=>array("en"=>"there is already an active poll", "ru" =>"сушествует активное голосование"));
		}
		return array("insertedevent" => -1,"insertedescription"=>array("en"=>"access denied", "ru" =>"отказ доступ"));
	}
	public function resetEvent($eventId, $phase1duration, $phase2duration){
		if($this->authenticated){
			$now = time();
			$activeEvents = $this->getEvents(false, " where $now < et ");
			if(!$activeEvents){
				$eventduration = $phase1duration + $phase2duration;
				$eventauthor = $this->myId;
				$pct = $now + $phase1duration;
				$et = $now + $eventduration;
				$MSQL_MAX_INT = 2147483647;
				queryMysql("delete from votes where eventid='$eventId'");

				$sql = "update event set eventduration='$eventduration',st='$now',pct='$pct',et='$et' where eventid='$eventId' and eventauthor='$eventauthor'";
				$q = queryMysql($sql);
				return array("insertedevent" => mysqli_affected_rows($q["link"]) > 0, "insertedescription"=>array("en"=>"succesfully refreshed poll", "ru" =>"успешно возобновил голосовоние"));
			}
			return array("insertedevent" => 0,"insertedescription"=>array("en"=>"there is already an active poll", "ru" =>"сушествует активное голосование"));
		}
		return array("insertedevent" => -1,"insertedescription"=>array("en"=>"access denied", "ru" =>"отказ доступ"));
	}
	public function getEvents($mine=true,$where=""){
		if($this->authenticated){
			//(eventid INT AUTO_INCREMENT PRIMARY KEY, eventname VARCHAR(256), eventduration INT, eventauthor INT, st INT, pct INT, et INT)
			$now = time();
			if($mine)
				$sql = "select users.userid,users.username,eventid,eventname,eventduration,eventauthor,st,pct,et, IF('$now' >= pct, 2, 1) as phase, IF('$now' < et , 1, 0) as active, IF('$now' >= pct, et - '$now', pct - '$now') as timeleft from event join users on users.userid=event.eventauthor where eventauthor = $this->myid $where";
			else
				$sql = "select users.userid,users.username,eventid,eventname,eventduration,eventauthor,st,pct,et, IF('$now' >= pct, 2, 1) as phase, IF('$now' < et , 1, 0) as active, IF('$now' >= pct, et - '$now', pct - '$now') as timeleft from event join users on users.userid=event.eventauthor $where";
			$q  = queryMysql($sql);
			$events = array();
			while($row = mysqli_fetch_assoc($q["result"])){
				$row["eventname"] = removeTableSanity($row["eventname"]);
				$row["username"] = removeTableSanity($row["username"]);
				$events[] = $row;
			}
			return $events;
		}
		return array();
	}
	public function vote($event, $action, $time){
		//(voteid INT AUTO_INCREMENT PRIMARY KEY, eventid INT, userid INT, votedtime INT, action VARCHAR(256))
		if($this->authenticated){
			$now = time();
			$id = $this->myId;
			$sql = "insert into votes (eventid, userid, votedtime, action) values ('$event','$id', '$time','$action') on duplicate key update voteid=voteid";
			$q = queryMysql($sql);
			return array("voted" => mysqli_insert_id($q["link"]) > 0, "message" => array("en" => (mysqli_insert_id($q["link"]) > 0)?"voted succesfully $action $time:00":"one person one vote!", "ru" =>(mysqli_insert_id($q["link"]) > 0)? "голосование успешно $action $time:00":"один человек один голос!"));
		}
		return array("voted" => 0);
	}
	public function updateVote($action,$time,$eventid){
		if($this->authenticated){
			$id = $this->myId;
			$sql = "update votes set votedtime='$time', action='$action' where eventid='$eventid' and userid='$id'";
			$q = queryMysql($sql);
			$updated = mysqli_affected_rows($q["link"]);
			if($updated)
				return array("voted" => $updated, "message" => array("en" =>"voted succesfully $action $time:00", "ru" =>"голосование успешно $action $time:00"));
			return $this->vote($eventid, $action, $time);//user had not participated
		}
		return -1;
	}
	public function getSavedActions(){
		if($this->authenticated){
			//(voteid INT AUTO_INCREMENT PRIMARY KEY, eventid INT, userid INT, votedtime INT, action VARCHAR(256))
			$sql = "select distinct action from votes join event on event.eventid=votes.eventid where 1 ";
			$q  = queryMysql($sql);
			$events = array();
			while($row = mysqli_fetch_assoc($q["result"])){
				$row["action"] = removeTableSanity($row["action"]);
				$events[] = $row;
			}
			return $events;
		}
		return array();
	}
	public function pollActive($event){
		$now = time();
		$sql = "select eventid from event where eventid='$event' and et > '$now'";
		$q = queryMysql($sql);
		return mysqli_num_rows($q["result"]);
	}
	public function getPollDetails(){
		$time = mktime(0, 0, 0, date('n'), date('j'));
		$per = time()%24*3600;
		$sql = "select eventid as eid,eventname, users.username from event join users on event.eventauthor=users.userid order by eid desc limit 10";
		$q  = queryMysql($sql);
		$events = array();
		while($row = mysqli_fetch_assoc($q["result"])){
			$row["eventname"] = removeTableSanity($row["eventname"]);
			$row["username"] = removeTableSanity($row["username"]);
			$events[] = $row;
		}
		return $events;
	}
	public function getNumEmployees(){
		$sql = "select count(userid) as numemplyees from users where 1";
		$q = queryMysql($sql);
		return mysqli_fetch_row($q["result"])[0];
	}
	public function getCurrentPollDetails(){
		$tm = time();
		$sql = "select eventid as eid,(select count(voteid) from votes where votes.eventid=eid) as voterturnout, (select count(userid) from users where 1) as numemplyees from event where et >'$tm'";
		$q  = queryMysql($sql);
		$events = array();
		$events = mysqli_fetch_assoc($q["result"]);
		return $events;
	}
	public function checkrunningpoll(){
		$tm = time();
		$sql = "select eventid from event where et > '$tm'";
		$q = queryMysql($sql);
		return (mysqli_num_rows($q["result"]) > 0)?mysqli_fetch_row($q["result"]):0;
	}
}
 ?>
