<?php
$dbhost = "127.0.0.1";
$dbname = "tendai_eventspoll";
$dbuser ="tendai_swed";
$dbpass = "%A9]+pL754^%#@%*(923)53";
$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
function queryMysql($query){
	global $mysqli;
	$result = mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
	$link = $mysqli;
	return compact('result', 'link');
}
function sanitizeString($var){
	global $mysqli;
	$var = strip_tags($var);
	$var = htmlentities($var, ENT_QUOTES, 'UTF-8');
	$var = stripslashes($var);
	$var = mysqli_real_escape_string($mysqli,$var);
	return $var;
}
function removeTableSanity($text){
	$text=preg_replace('/&amp;#039;/',"'",$text);
	$text=preg_replace('/&amp;quot;/','"',$text);
	$text=preg_replace('/&amp;gt;/',">",$text);
	$text=preg_replace('/&amp;lt;/',"<",$text);
	$text=preg_replace('/&amp;/',"&",$text);
	return $text;
}
function passHash($var){
	$options = [
    'cost' => 12,
	];
 	return password_hash($var, PASSWORD_BCRYPT, $options);
}
function checkPasswordHash($hash,$password){
      return (password_verify($password, $hash));
}
function removeSpace($name){
	$name= preg_replace( "{\s+}", '', $name);
	$name = strtoupper($name);
	return $name;
}
function nameReturn($name,$unique_key){
	$output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = hash("ripemd128",$unique_key);
    $secret_iv = hash("md5",$unique_key);
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
	$output = openssl_decrypt(base64_decode($name), $encrypt_method, $key, 0, $iv);
	return $output;
}
function nameDisguise($name,$unique_key){
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = hash("ripemd128",$unique_key);
    $secret_iv = hash("md5",$unique_key);
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
	$output = openssl_encrypt($name, $encrypt_method, $key, 0, $iv);
	$output = base64_encode($output);
    return $output;
}
function cmpArraysBySize($a, $b, $desc = true){
    if (count($a) == count($b)) {
        return 0;
    }
		if($desc)
    	return (count($a) < count($b)) ? 1 : -1;
		return (count($a) < count($b)) ? -1 : 1;
}


 ?>
