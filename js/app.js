var g__lang = "ru";
var g__pollison = 0;
var g__lan_st = new storage()

if(isDefined(g__lan_st.get("language")))
	g__lang = g__lan_st.get("language");

function split( val ) {
	return val.split( /,\s*/ );
}
function extractLast( term ) {
	return split( term ).pop();
}
var getAJAXRequests = (function() {
    var oldSend = XMLHttpRequest.prototype.send,
        currentRequests = [];

    XMLHttpRequest.prototype.send = function() {
        currentRequests.push(this); // add this request to the stack
        oldSend.apply(this, arguments); // run the original function

        // add an event listener to remove the object from the array
        // when the request is complete
        this.addEventListener('readystatechange', function() {
            var idx;

            if (this.readyState === XMLHttpRequest.DONE) {
                idx = currentRequests.indexOf(this);
                if (idx > -1) {
                    currentRequests.splice(idx, 1);
                }
            }
        }, false);
    };

    return function() {
        return currentRequests;
    }
}());

function formatTime(time){
	//get time in seconds and formart it in hh:mm:ss
	var s = time%60;
	time = Math.floor(time/60);
	var m = time%60;
	time = Math.floor(time/60);
	var h = time%60;
	return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}

function localization(){
	var date = new Date();
	date = date.getFullYear();
	var st = new storage();
	if(isDefined(st.get("g__lang")))
		g__lang = st.get("g__lang");
}
function isDefined(variable){
	return !(typeof variable === 'undefined' || variable===null);
}
function checkStorage(){
	if(!navigator.cookieEnabled)
		window.location.replace("error/storage/");
}
function storage(){ //caters for browsers that even dont support sessionStorage by use of sessionstorage.min.js
	checkStorage()
	this.set = function(key,value){
    sessionStorage.setItem(key,value);
  }
  this.get = function(key){
    return sessionStorage.getItem(key);
  }
	this.reset = function(){
		sessionStorage.clear();
	}
}

jQuery.fn.extend({
	acCtrl : function(){
		this.bind( "keydown", function( event ) {
			if ( event.keyCode === $.ui.keyCode.TAB &&
					$( this ).data( "ui-autocomplete" ).menu.active ) {
				event.preventDefault();
			}
		})
		.autocomplete({
			minLength: 1,
			source: function( request, response ) {
				var st = new storage();
				var req = $().ajaxApi("actions/",{"token" : st.get("token")})
        req.done(function(data){
					st.set("token",data.token)
					response(data.data);
				})
				req.fail(function(e){
					console.log(e)
				})
      },
			focus: function() {
				// prevent value inserted on focus
				return false;
			},
			select: function( event, ui ) {
				var terms = split( this.value );
				// remove the current input
				terms.pop();
				// add the selected item
				terms.push( ui.item.value );
				// add placeholder to get the comma-and-space at the end
				terms.push( "" );
				this.value = terms.join( "" );
				return false;
			}
		});
	},
	vote : function(payload){
		var st = new storage();
		var req = this.ajaxApi("vote/",payload);
		req.done(function(data){
			if(data.authenticated){
				var st = new storage();
				st.set("token",data.token)
				if(data.data.response.voted == 0)
					$().alertBar("danger",[{"language":"en", "info": data.data.response.message.en}, {"language": "ru", "info": data.data.response.message.ru}]);
				if(data.data.response.voted == 1)
					$().alertBar("success",[{"language":"en", "info": data.data.response.message.en}, {"language": "ru", "info": data.data.response.message.ru}]);
			}
		});
		req.fail(function(e){
			console.log(e)
		})
	},
	updateTimeOptions : function(){
		var st = new Date().getHours();
		this.find("option").map(function(val,f){
			if($(f).prop("value") < st)
				$(f).remove();
		})
	},
	getleftpollmenu : function(){
		var req = this.ajaxApi("getallpolls/");
		var that = this;
		var username = $(".username").text();
		req.done(function(data){
			var html = 	'<a class="list-group-item loadeventid" id="link_to_activate_0" data-view="start-poll">'
					html += '<span class="multi-lang ru"> инициировать голосование	</span>'
					html += '<span class="multi-lang en"> initialize voting </span>'
					html += '</a>';
			var dd = data.data;
			for(var i in dd){
				var addhtml =""
				if(username==dd[i].username)
					addhtml = "<i class='fa fa-refresh'></i>";
				html += '<a class="list-group-item list-group-item-action loadeventid" id="link_to_activate_'+dd[i].eid+'" data-view="'+dd[i].eid+'"><span class="float-left">'+dd[i].eventname+'</span><span class="float-right">'+addhtml+'</span></a>';
			}
			that.html(html);
			that.find("#link_to_activate_"+g__pollison).addClass("active");//let user know the active poll
			$().localize();
		})
		req.fail(function(e){
			console.log(e)
		})
	},
	permanantlyoppose : function(){
		$(".polling-2-vote").addClass("hidden");
		var st = new storage(false);
		st.set("opposedpermanently",$("#permanantlyoppose-id").val()) //phase two is short lived to be saveable in session storage
	},
	phasetwo :function(){ //offering opportunity to change vote
		var data = arguments[0];
		if(data.pollactive){
			var username = $(".username").text();
			var st = new storage(false);
			$("#input-action-second-phase").val(data.actionwinner);
			$("#input-st-time-second-phase").val(data.timewinnerint);
			if(data.actionwinnerlist.length && data.timewinnerlist.list){
				if(st.get("opposedpermanently") != $("#permanantlyoppose-id").val()){//the denied vote is the same with this active phase 2 poll, opposedpermanently can be null
					if(!data.actionwinnerlist.includes(username)){//OFFER TO JOIN THE WINNING POLL
						$(".polling-2-vote").removeClass("hidden");
					}else if(!data.timewinnerlist.includes(username)){
						$(".polling-2-vote").removeClass("hidden");
					}
				}
			}
		}
		return;
	},
	secondphase : function(){ //sending input from accept results on phase 2
		var req = $().ajaxApi("secondphase/",arguments[0]);
		var st = new storage();
		req.done(function(data){
			if(data.authenticated){
				$().loadActivePoll();
			}
		})
		req.fail(function(e){
			console.log(e)
		})
	},
	resetpoll : function(payload){
		var st = new storage();
		var req = this.ajaxApi("resetpoll/",payload);
		req.done(function(data){
			if(data.authenticated){
				var st = new storage();
				st.set("token",data.token)
				if(data.data.response.insertedevent >= 0){
					location.reload();
				}else if (data.data.response.insertedevent == -1) { //no access
					var st = new storage();
					st.reset();
				}
			}
		});
		req.fail(function(e){
			console.log(e)
		})
	},
	startpoll : function(payload){
		var st = new storage();
		var req = this.ajaxApi("startpoll/",payload);
		req.done(function(data){
			if(data.authenticated){
				var st = new storage();
				st.set("token",data.token)
				if(data.data.response.insertedevent >= 0){
					location.reload();
				}else if (data.data.response.insertedevent == -1) { //no access
					var st = new storage();
					st.reset();
				}
			}
		});
		req.fail(function(e){
			console.log(e)
		})
	},
	alertBar : function(cls,info){
		var html = '<div class="alert alert-'+cls+' col-12 m-0" role="alert">';
		for(var i in info){
			html += '<span class="multi-lang '+info[i]["language"]+'">'+info[i].info+'</span>'
		}
		html += '</div>';
		$(".alert-bar").html(html)
		$().localize();
	},
	localize : function(){
		$(".select-langs").val(g__lang);
		$(".multi-lang").css("display","none");
		$("."+g__lang).css("display","block");
	},
	checkrunningpoll : function(){
		var notify = arguments[0];

		var checkPolls = setInterval(function(){
			var req = $().ajaxApi("checkrunningpoll/");
			req.done(function(data){
				if(data){
					if(!notify)
						$().loadActivePoll();
					else {
						var st = new storage();
						if(data[0]!=st.get("activepoll"))
							$().alertBar("danger",[{"language":"en", "info": "a poll is in progress"}, {"language": "ru", "info": "идет голосование"}]);
					}
				}
			})
			req.fail(function(e){
				console.log(e)
			})

		},3000)
	},
	loadActivePoll : function(){
		var st = new storage();
		var req;
		function workdata(data){
			if(g__pollison)
				$(".all-events-links").find("#link_to_activate_"+g__pollison).addClass("bg-info");
			$(".toggle-main-view").addClass("hidden");
			if(data.authenticated){
				st.set("token",data.token)
				st.set("username",data.username)
				$("#login-now-modal").modal("hide")
				$(".my-token").val(data.token)
				if(data.data.active.length){ //there is an active or archived poll
					$(".event-title").text(data.data.active[0]["eventname"]);
					$(".input-event-id").val(data.data.active[0]["eventid"])
					$(".all-events-links").find("a").removeClass("active")
					$(".all-events-links").find("#link_to_activate_"+data.data.active[0]["eventid"]).addClass("active");
					//console.log(data.data.active[0].timeleft)
					var isactivepoll = (data.data.active[0].timeleft>0)?true:false;
					if(isactivepoll){ // poll is active
						g__pollison = data.data.active[0]["eventid"];

						$(".countdown").countdownTime(data.data.active[0]["timeleft"],$(".polls-counter"),data.data.active[0].phase); // update the countdown and the votes as they come in
						$(".all-events-links").find("#link_to_event_"+data.data.active[0]["eventid"]).addClass("polls-counter")
						//console.log($(".all-events-links").find("#link_to_event_"+data.data.active[0]["eventid"]),$(".all-events-links").find("#link_to_activate_"+data.data.active[0]["eventid"]),data.data.active[0]["eventid"])
						st.set("activepoll",data.data.active[0]["eventid"])
						$(".event-phase").text(data.data.active[0]["phase"])
						setTimeout(function(){
							location.reload();
						},data.data.active[0]["timeleft"]*1000)
						if(data.data.active[0].phase == 2){
							$().alertBar("warning",[{"language":"en", "info": "second phase of poll"}, {"language": "ru", "info": "вторая фаза голосования"}]);
							//console.log(data.data.active)
							//get poll results in the countdown refresher for phase 2
							drawPieChart("chart_div",data.data.active[0]["eventid"]);
							$(".polling-2").removeClass("hidden");
						}else {
							$("#input-action").acCtrl()//autocomplete helper
							$(".polling").removeClass("hidden");
						}
						//end of active poll
					}else{ //load history of poll
						//get poll results
						//allow restart poll if author
						var username = $(".username").text();
						if(username == data.data.active[0].username && !g__pollison){
							$("#resetpoll-input-pollid").val(data.data.active[0]["eventid"])
							$(".reset-poll").removeClass("hidden");
							$("#resetpoll-input-pollname").val(data.data.active[0].eventname)
						}

						$().alertBar("primary",[{"language":"en", "info": "resuts to poll"}, {"language": "ru", "info": "результаты голосования"}]);
						if(g__pollison!==data.data.active[0]["eventid"]){
							$().checkrunningpoll(true);//will notify that a new poll has been initiated
							drawPieChart("chart_div",data.data.active[0]["eventid"]); //will ajax it's datasource
						}

						$(".results").removeClass("hidden")
					}
				}else {//start a new poll
					//check every 3 seconds if someone started a poll, if true load this poll asap
					$().alertBar("primary",[{"language":"en", "info": "you can start a poll"}, {"language": "ru", "info": "Вы момжете начинать голосование"}]);
					if(!g__pollison){
						$().checkrunningpoll();
						$(".start-poll").removeClass("hidden");
					}
				}

			}else{
				if($('#login-now-modal').is(':visible')){
					$("#login-signup-alert-fail").text(data.error[g__lang])
					$("#login-signup-alert-fail").removeClass("hidden")
				}else {
					if(!getAJAXRequests().length){//in case requests are faster than the ajax
						$().loginprompt()
					}
				}
			}
		}

		if(!arguments[0]){//a call from second phase polling leads here
		//greab the information as to if I have participated in the poll and have voted for the winner
				var req = this.ajaxApi("getpoll/",{"token" : st.get("token")});
				req.done(function(data){
					if(!g__pollison)
						$(".all-events-links").getleftpollmenu(); //in history needn't reload list because it breaks the active link
					workdata(data)
				});
				req.fail(function(e){
					console.log(e)
				})
		}else {//got the data already
			if(typeof(arguments[0])=="object"){
				if(!g__pollison)
					$(".all-events-links").getleftpollmenu();
				workdata(arguments[0])
			}else{//loading particular poll history view
				//arguments[0] is event id
				var req = this.ajaxApi("getpoll/",{"token" : st.get("token"), "event": arguments[0]});
				req.done(function(data){
					workdata(data)
				});
				req.fail(function(e){
					console.log(e)
				})
			}
		}
	},
  ajaxApi : function(api, payload){
    if(typeof(arguments[2])=="function")//callback
      arguments[2].apply(this);
    var req = $.ajax({
      url: "api/"+api,
      method: "POST",
      data: payload,
      dataType: "json"
    })
    return req;
  },
	countdownTime : function(val){
		var that = this;
		var votecounter = arguments[1];
		var phase = arguments[2]
		var numemplyees = 1;
		var voterturnout = 0;
		var initialval = val;
		var ctr = setInterval(function(){
			if(val>-1){
				var clss = "text-success";
				if(val < 120)
					clss ="text-warning"
				if(val < 30)
				 	clss = "text-danger"
				that.html("<h3 class='"+clss+"'>"+formatTime(val--)+"</h3>")
				if((val%3==0 || val==initialval) && voterturnout != numemplyees){//update the votes in element in arguments[1] every 3 seconds until 100% voter turnout or timeout
					var req = votecounter.ajaxApi("votecount/",{});
					req.done(function(data){
						if(voterturnout!= data.data.voterturnout && phase == 2) //need for refresh pie ar
							drawPieChart("chart_div",data.data["eid"]); //will ajax it's datasource

						voterturnout = data.data.voterturnout;
						numemplyees = data.data.numemplyees;
						var clss = "text-success";
						if(voterturnout/numemplyees >= 0.5)
							clss ="text-warning"
						if(voterturnout/numemplyees < 0.5)
						 	clss = "text-danger"
						votecounter.html("<h3 class='"+clss+"'>"+voterturnout+"/"+numemplyees+"</h3>")
					})
					req.fail(function(e){
						console.log(e)
					})
				}
			}else{
				closeCountDown();
			}
		},1000)
		function closeCountDown(){
			g__pollison = 0;
			clearInterval(ctr)
		}

	},
	login : function(payload){
		var req = this.ajaxApi("login/",payload);
		req.done(function(data){
			if(data.authenticated){
				$(".username").html(data.username)
				$().loadActivePoll(data)
			}else{
				if($('#login-now-modal').is(':visible')){
					$("#login-signup-alert-fail").text(data.error[g__lang])
					$("#login-signup-alert-fail").removeClass("hidden")
				}else
					$().loginprompt()
			}
		});
		req.fail(function(e){
			console.log(e)
		})
	},
	checkuser : function(user){ //apply to the form instead of the input
		var req = this.ajaxApi("checkuser/",{"user":user});
		var that = this;
		req.done(function(data){
			that.find("button.submit-form").prop("disabled",true);
			that.find("i.username-checks").addClass("hidden")
			that.find("i.username-checks").addClass("hidden")
			if(data)
				that.find("i.username-taken").removeClass("hidden")
			else{
				that.find("button.submit-form").prop("disabled",false);
				that.find("i.username-free").removeClass("hidden")
			}

		});
		req.fail(function(e){
			console.log(e);
		});
	},
	signup : function(payload){
		var req = this.ajaxApi("signup/",payload);
		req.done(function(data){
			if(data.authenticated){
				$().loadActivePoll(data)
				$(".username").html(data.username)
			}else{
				if($('#login-now-modal').is(':visible')){
					$("#login-signup-alert-fail").text(data.error[g__lang])
					$("#login-signup-alert-fail").removeClass("hidden")
				}else
					$().loginprompt()
			}
		})
		req.fail(function(e){
			console.log(e)
		})
	},
	authenticated : function(){
		var st = new storage();
		if(isDefined(st.get("token"))){
			this.login({"token" : st.get("token")});
				if(typeof(arguments[0]) == "function")
					arguments[0].appy();
		}else{
			$().loginprompt()
		}
	},
	loginprompt : function(){
		$("#login-now-modal").modal({
			show : true,
			keyboard: false,
			focus: true,
			backdrop:'static'
		})
	},
	showResults : function(heading,list){
		var html = '<div class="card bg-primary col-12">';
				html += '	<div class="card-body">'
				html +=    heading;
				html += '	</div>';
				html += '</div>'
		for(var i in  list){
			html += '<div class="card p-0">';
			html += '	<div class="card-body p-2">';
			html +=     list[i];
			html += '	</div>';
			html += '</div>'
		}
		this.html(html);
	}
});

$(function(){
	//render chart then update the dataset as data changes
	$().localize();
	$().authenticated();
	formsCtrl();
	initTimeSelect();
	$("#registration-name-input").on("change",function(){
		$("#signup").checkuser($(this).val())
	})
	$(".all-events-links").on("click",".loadeventid",function(){
		if($(this).data("view")!="start-poll"){
			$().loadActivePoll($(this).data("view"));
		}else if (!g__pollison) {
			$().loadActivePoll();
		}
		$(".loadeventid").removeClass("active")
		$(this).addClass("active");
	})
	$(".copyright").html(new Date().getFullYear())
	$("#input-st-time").on("click",function(){
		$(this).updateTimeOptions()
	});
	$(".select-langs").on("change",function(){
		var st = new storage();
		g__lang = $(this).val();
		st.set("language",g__lang)
		$().localize();
	});
	function initTimeSelect(){
		for(var i = 1; i <24; ++i){
				$("#input-st-time").append("<option value="+i+">"+i+":00</option>")
		}
		$("#input-st-time").updateTimeOptions();
	}
	function formsCtrl(){
		$("body").on("submit","form",function(e){
			e.preventDefault();
			var api = $(this).prop("id")
			var st = new storage();
			$()[api]($(this).serialize()+"&&token="+st.get("token"))
		})
	}
});
function drawDougnut(doughnutdata,doughnutcontainer){
			google.charts.load("current", {packages:["corechart"]});
			google.charts.setOnLoadCallback(drawChart);
			function drawChart() {
				var data = new google.visualization.DataTable();
				var titles = {"en": "results", "ru" : "результаты"}
				var columns = [
				{"type": "string", "value": "actions"},
				{"type": "number", "value": "numvotes"}
				]
				for(var i in columns)
					data.addColumn(columns[i].type,columns[i].value);
				data.addRows(doughnutdata);
				var options = {
					'title':titles[g__lang],
					pieHole: 0.6,
					'backgroundColor': 'transparent',
					'pieSliceText': 'label',
					'legend':{position: 'right', alignment:'end'},
					'chartArea':{width:'100%',height:'100%',left:'0px',top:'0px',right:'0px',bottom:'0px'},
				};

				var chart = new google.visualization.PieChart(document.getElementById(doughnutcontainer));

				/*console.log(cli.getChartAreaBoundingBox().left)
				console.log(cli.getChartAreaBoundingBox().top)
				console.log(cli.getChartAreaBoundingBox().height)
				console.log(cli.getChartAreaBoundingBox().width)*/
				google.visualization.events.addListener(chart, 'onmouseover', function(){
					//console.log(this)
					//consol.log(chart)
				});
				chart.draw(data, options);
			}
}
function drawPieChart(piecontainer,pollid) {
    var req = $.ajax({
        url: 'api/getpiedata/?event='+pollid,
        dataType: 'json'
    });
		req.fail(function(e){
			console.log(e)
		})
		req.done(function(piedata){
				$().phasetwo(piedata)
				if(!piedata.timewinnerlist.length && !piedata.actionwinnerlist.length)
					$(".noone-voted").removeClass("hidden")
				$(".winning-time-voters").showResults(piedata.timewinner,piedata.timewinnerlist);
				$(".winning-action-voters").showResults(piedata.actionwinner,piedata.actionwinnerlist);
				var notvoted = {"en":"no vote","ru":"без голосования"}
				if(piedata.pie[0][0] == "-")
					piedata.pie[0][0] = notvoted[g__lang];
				if(piedata.doughnut[0][0] == "-")
					piedata.doughnut[0][0] = notvoted[g__lang];
				drawDougnut(piedata.doughnut,piecontainer+"_d");
				google.charts.load('current', {'packages':['corechart']});
				// Set a callback to run when the Google Visualization API is loaded.
				google.charts.setOnLoadCallback(drawPieChart);
				// Callback that creates and populates a data table,
				// instantiates the pie chart, passes in the data and
				// draws it.
				function drawPieChart() {
					var columns = [
					{"type": "string", "value": "times"},
					{"type": "number", "value": "numvotes"}
					]

					// Create the data table.
					var data = new google.visualization.DataTable();
					for(var i in columns)
						data.addColumn(columns[i].type,columns[i].value);

					data.addRows(piedata.pie);
					// Set chart options
					var options = {
												//'backgroundColor': 'transparent', should cover loading message after successful loading
												'chartArea':{width:'100%',height:'100%',left:'0px',top:'0px',right:'0px',bottom:'0px'},
												'pieSliceText': 'label',
												'pieSliceTextStyle': {'text-align': 'center', color:'black'}
											};
					// Instantiate and draw our chart, passing in some options.
					var chart = new google.visualization.PieChart(document.getElementById(piecontainer));
					chart.draw(data, options);
				}
			});
}
