#assumption the initiator has and admin account in mysql on localhost
CREATE USER 'tendai_swed'@'localhost' IDENTIFIED BY '%A9]+pL754^%#@%*(923)53';
\! echo "*** created database user tendai_swed ***"
CREATE DATABASE IF NOT EXISTS tendai_eventspoll;
\! echo "*** created table database tendai_eventspoll ***"
use tendai_eventspoll;
CREATE TABLE IF NOT EXISTS users (userid INT AUTO_INCREMENT PRIMARY KEY, username VARCHAR(128), password VARCHAR(256), hash VARCHAR(256), unique(username))ENGINE InnoDB;
\! echo "*** created table users ***"
DESCRIBE users;
\! echo
CREATE TABLE IF NOT EXISTS event (eventid INT AUTO_INCREMENT PRIMARY KEY, eventname VARCHAR(256), eventduration INT, eventauthor INT, st INT, pct INT, et INT)ENGINE InnoDB;
\! echo "*** created table event ***"
DESCRIBE event;
\! echo
CREATE TABLE IF NOT EXISTS votes (voteid INT AUTO_INCREMENT PRIMARY KEY, eventid INT, userid INT, votedtime INT, action VARCHAR(256), unique(eventid,userid)) ENGINE InnoDB;
\! echo "*** created table votes ***"
DESCRIBE votes;
\! echo
GRANT INSERT,SELECT,UPDATE ON tendai_eventspoll.* TO `tendai_swed`@`localhost`;
GRANT INSERT,SELECT,UPDATE,DELETE ON tendai_eventspoll.votes TO `tendai_swed`@`localhost`;
