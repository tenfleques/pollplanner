<?php
include_once("../../bin/user.php");

if(isset($_POST["token"])){
  $token = sanitizeString($_POST["token"]);
  $user = new user();
  $json = $user->authenticate($token);
  if($json["authenticated"]){
      $now = time();
      if(isset($_POST["resetpoll-input-pollid"])){
        $eventname = sanitizeString($_POST["resetpoll-input-pollid"]);
        $phase1duration = sanitizeString($_POST["resetpoll-input-st"]) * 60;
        $phase2duration = sanitizeString($_POST["resetpoll-input-pct"]) * 60;
        $json["data"]["response"] = $user->resetEvent($eventname, $phase1duration, $phase2duration);
        $json["error"] = array("en"=>"success", "ru"=> "успешно");
      }else{
        $json["data"] = $user->getEvents(false,"where et > '$now'");
        $json["data"] = $json["data"][0];
        $json["error"] = array("en"=>"success", "ru"=> "успешно");
      }
  }else{
    $json["error"] = array("en"=>"access denied", "ru"=> "отказ доступа");
  }
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($json,JSON_UNESCAPED_UNICODE,JSON_NUMERIC_CHECK);
?>
