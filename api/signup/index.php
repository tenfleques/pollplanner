<?php
include_once("../../bin/user.php");
$json = array("authenticated" => false, "token" => "", "username" => "", "error" =>array("en"=>"error", "ru"=> "ошибка"));
if(isset($_POST["registration-name"])){
  if($_POST["bot-ctrl"]!=="xxx")
    exit;
  $username = sanitizeString($_POST["registration-name"]);
  $pass = sanitizeString($_POST["registration-pass"]);
  $confirmpass = sanitizeString($_POST["registration-pass-confirm"]);
  if($pass == $confirmpass){
    if($username && $pass){
      $user = new user();
      $json = $user->signUp($username,$pass);
    }else{
      $json = array("authenticated" => false, "token" => "", "username" => "", "data"=>array(), "error" =>array("en"=>"missing data", "ru"=> "необходимые информации отсуствует"));
    }
  }else{
    $json = array("authenticated" => false, "token" => "", "username" => "", "data"=>array(), "error" =>array("en"=>"passwords mismatch", "ru"=> "пароли не совпадают"));
  }
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($json,JSON_UNESCAPED_UNICODE);
?>
