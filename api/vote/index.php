<?php
include_once("../../bin/user.php");

if(isset($_POST["token"])){
  $token = sanitizeString($_POST["token"]);
  $user = new user();
  $json = $user->authenticate($token,false);
  if($json["authenticated"]){
      $now = time();
      if(isset($_POST["input-action"])){
        $action = sanitizeString($_POST["input-action"]);
        $time = sanitizeString($_POST["input-st-time"]);
        $eventid = sanitizeString($_POST["input-event"]);
        $json["data"]["response"] = $user->vote($eventid,$action,$time);
        $json["error"] = array("en"=>"success", "ru"=> "успешно");
      }
  }else{
    $json["error"] = array("en"=>"access denied", "ru"=> "отказ доступа");
  }
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($json,JSON_UNESCAPED_UNICODE,JSON_NUMERIC_CHECK);
?>
