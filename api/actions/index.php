<?php
include_once("../../bin/user.php");

if(isset($_POST["token"])){
  $token = sanitizeString($_POST["token"]);
  $user = new user();
  $json = $user->authenticate($token,false);
  if($json["authenticated"]){
      $now = time();
      foreach($user->getSavedActions() as $section => $item)
        $json["data"][] = $item["action"];
      $json["debug"] = "accessed";
  }else
    $json["debug"] = "no access";
}else {
  $json["debug"] = "no token";
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($json,JSON_UNESCAPED_UNICODE,JSON_NUMERIC_CHECK);
?>
