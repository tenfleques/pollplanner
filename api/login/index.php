<?php
include_once("../../bin/user.php");

if(isset($_POST["login-name"])){
  if($_POST["bot-ctrl"]!=="yyy")
    exit;
  $username = sanitizeString($_POST["login-name"]);
  $pass = sanitizeString($_POST["login-pass"]);
  if($username && $pass){
    $user = new user();
    $json = $user->login($username,$pass);
  }else{
    $json = array("authenticated" => false, "token" => "", "username" => "", "data"=>array(), "error" =>array("en"=>"missing data", "ru"=> "необходимые информации отсуствует"));
  }
}
elseif(isset($_POST["token"])){
  $token = sanitizeString($_POST["token"]);
  $user = new user();
  $json = $user->authenticate($token);
  if($json["authenticated"]){
    $now = time();
    $json["data"] = array("active"=>$user->getEvents(false,"where et > '$now'"));
    $json["error"] = array("en"=>"success", "ru"=> "успешно");
  }
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($json,JSON_UNESCAPED_UNICODE);
?>
