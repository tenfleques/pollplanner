<?php
include_once("../../bin/user.php");

if(isset($_POST["token"])){
  $token = sanitizeString($_POST["token"]);
  $user = new user();
  $json = $user->authenticate($token,false);//no refresh
  if($json["authenticated"]){
      if(isset($_POST["input-action-second-phase"])){
        $action = sanitizeString($_POST["input-action-second-phase"]);
        $time = sanitizeString($_POST["input-st-time-second-phase"]);
        $eventid = sanitizeString($_POST["input-event-second-phase"]);
        $json["data"]=array("updated"=>$user->updateVote($action,$time,$eventid));
        $json["error"] = array("en"=>"success", "ru"=> "успешно");
      }
  }else{
    $json["error"] = array("en"=>"access denied", "ru"=> "отказ доступа");
  }
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($json,JSON_UNESCAPED_UNICODE,JSON_NUMERIC_CHECK);
?>
