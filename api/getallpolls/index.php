<?php
include_once("../../bin/user.php");

$user = new user();
$now = time();
$json["data"] = $user->getPollDetails();
header('Content-Type: application/json; charset=utf-8');
echo json_encode($json,JSON_UNESCAPED_UNICODE,JSON_NUMERIC_CHECK);
?>
