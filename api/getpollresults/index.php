<?php
include_once("../../bin/user.php");
if(isset($_POST["event"])){
  $json["id"] = $eventId = sanitizeString($_POST["event"]);
  $user = new user();
  $now = time();
  $json["data"] = $user->getPollResults($eventId);
}else {
  $json["error"] = "no id";
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($json,JSON_UNESCAPED_UNICODE,JSON_NUMERIC_CHECK);
?>
