<?php
include_once("../../bin/user.php");
$json = array("pie"=>array(),"doughnut"=>array(), "actionwinnerlist"=>array(), "timewinnerlist"=>array(), "pollactive"=>false);
function getWinnerList($freq,&$winner,$eventauthor,$it,&$win,$winningvalue){
  //not given in question but in the event of tie, privilledge to the poll author
  if($freq > count($winner)){
    $win = $it[0][$winningvalue];
    $winner = array_map(function($var){
      return $var["username"];
    },$it);
  }elseif ($freq == count($winner)) { //choose one containing the user
    if(!in_array($eventauthor,$winner)){
      $winner = array_map(function($var){
        return $var["username"];
      },$it);
      $win = $it[0][$winningvalue];
    }
  }
}
if(isset($_GET["event"])){
  $user = new user();
  $eventId = sanitizeString($_GET["event"]);
  $arr = $user->getPollResults($eventId);
  $eventauthor = $user->getEventAuthor($eventId);

  $voters = 0;
  $allemployees = $user->getNumEmployees();
  $timewinnerlist = array();
  $actionwinnerlist = array();
  $json["actionwinner"]=false;
  $json["timewinner"] ="";
  $json["timewinnerint"]=false;
  $json["pollactive"] = $user->pollActive($eventId);

  foreach($arr["times"] as $sec => $it){
    $freq = count($it);
    $voters += $freq;
    if($json["timewinnerint"]===false)
      $json["timewinnerint"] = $it[0]["votedtime"];//to initialize value
    getWinnerList($freq,$timewinnerlist,$eventauthor,$it,$json["timewinnerint"],"votedtime");
    if($it[0]){
      $json["pie"][] = array($it[0]["votedtime"].":00",$freq);
      foreach($it as $item => $var){
        $json["allvoters"][] = $var["username"];
      }
    }
  }


  foreach($arr["actions"] as $sec => $it){
    $freq = count($it);
    if($json["actionwinner"]===false)
      $json["actionwinner"] = $it[0]["action"];
    getWinnerList($freq,$actionwinnerlist,$eventauthor,$it,$json["actionwinner"],"action");
    if($it[0]){
      $json["doughnut"][] = array($it[0]["action"],$freq);
    }
  }
  $json["pie"][] = array("-",$allemployees-$voters);
  $json["doughnut"][] = array("-",$allemployees-$voters);
  $json["timewinner"] = $json["timewinnerint"].":00";
  $json["actionwinnerlist"] = $actionwinnerlist;
  $json["timewinnerlist"] = $timewinnerlist;
  header('Content-Type: application/json; charset=utf-8');
  echo json_encode($json,JSON_UNESCAPED_UNICODE,JSON_NUMERIC_CHECK);

}
?>
