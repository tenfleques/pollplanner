<?php
include_once("../../bin/user.php");
if(isset($_POST["user"])){
  $username = sanitizeString($_POST["user"]);
  $user = new user();
  $json = $user->userExists($username);
}
header('Content-Type: application/json; charset=utf-8');
echo json_encode($json,JSON_UNESCAPED_UNICODE,JSON_NUMERIC_CHECK);
?>
